package com.nineoldandroids.view;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;

/**
 * Created with IntelliJ IDEA.
 * User: kanedong
 */
public class AnimationDrawableHolder extends DrawableHolder {
    public AnimationDrawableHolder(AnimationDrawable drawable) {
        setDrawable(drawable);
    }

    @Override
    public void setDrawable(Drawable drawable) {
        super.setDrawable(drawable);
        if (null != drawable && drawable instanceof AnimationDrawable) {
            super.setDrawable(drawable);
            setupBounds();
        }
    }

    public void start() {
        if (null != mDrawable) {
            setupBounds();

            ((AnimationDrawable) mDrawable).start();
        }
    }

    private void setupBounds() {
        // TODO: 是否需要将所有帧的外包矩形计算出来？
        if (((AnimationDrawable) mDrawable).getNumberOfFrames() > 0) {
            Drawable drawable = ((AnimationDrawable) mDrawable).getFrame(0);
            mDrawable.setBounds(0, 0,
                    drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }
    }

    public void stop() {
        if (null != mDrawable) {
            ((AnimationDrawable) mDrawable).stop();
        }
    }

    public void isRunning() {
        if (null != mDrawable) {
            ((AnimationDrawable) mDrawable).isRunning();
        }
    }

    public void setOneShot(boolean isOneShot) {
        if (null != mDrawable) {
            ((AnimationDrawable) mDrawable).setOneShot(isOneShot);
        }
    }
}
